import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { JWTIntercepter } from './helper/jwtintercepter.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { GraphsComponent } from './graphs/graphs.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { NetworkComponent } from './network/network.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthGuard } from './helper/authguard';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    GraphsComponent,
    NetworkComponent,
    NavbarComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxChartsModule,
    NgbModule
  ],
  providers: [ AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JWTIntercepter, multi: true }, ],
  bootstrap: [AppComponent]
})
export class AppModule { }
