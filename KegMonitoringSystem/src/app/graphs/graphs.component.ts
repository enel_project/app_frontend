import {Component, OnInit, Input} from '@angular/core';
import { GraphService } from '../services/graph-service';
import { NetworkObject } from '../models/network';
@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.component.html',
  styleUrls: ['./graphs.component.css']
})
export class GraphsComponent implements OnInit {
    @Input('network') networkObject: NetworkObject;
  date;
  time: any = {};
  network: any;
  graphOption;
  timePeriod;
  view: any[]  = [575, 275];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = 'Date/Time';
  showYAxisLabel = false;
  yAxisLabel = 'Keg Name ';
  NameyAxisLabel = 'Name';
  NamexAxisLabel = 'Volume % ';
  BatteryxAxisLabel = '% left';
  minRadius = 3;
  maxRadius = 8;
  yScaleMin = 0;
  yScaleMax = 4;
 // yAxisTicks: any [] = [ 0, 1, 2, 3, 4, 5 ];

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  getToolTip(data) {
    console.log(data);
  }
  constructor(private graphservice: GraphService) {
   this.makeDate();
   this.graphOption = 'Total Sales';
   this.timePeriod = '1 Day';
}

  makeDate() {
    this.date = new Date();
    const dd = this.date.getDate();
    const mm = this.date.getMonth() + 1;
    const yyyy = this.date.getFullYear();
    this.date = {
      year: yyyy,
      month: mm,
      day: dd,
    };
  }

  onSelect(event) {
    // console.log(event);
  }

  onSubmit() {
    let tempDate = new Date();
    tempDate.setDate(this.date.day + 1);
    tempDate.setMonth(this.date.month - 1);
    tempDate.setFullYear(this.date.year);
    let tempTime;
    if (this.timePeriod === '1 Hour') {
      tempTime = '1h';
      const currentTime = new Date();
      tempDate = currentTime;
    } else if (this.timePeriod === '1 Day') {
    tempTime = '1d';
    tempDate.setHours(10);
    tempDate.setMinutes(0);
   } else if (this.timePeriod === '1 Week') {
     tempTime = '7d';
     tempDate.setHours(10);
    tempDate.setMinutes(0);
   } else {
    tempTime = '1y';
    tempDate.setHours(10);
    tempDate.setMinutes(0);
   }
    const object = {
      date: tempDate.getTime(),
      range: tempTime,
    };
    this.graphservice.networkGraph(object, this.networkObject.network).subscribe(data => {
      this.networkObject.AmountGraph(data);
      this.networkObject.BatteryGraph(data);
      this.networkObject.SaleGraph(data);
      this.networkObject.TrendGraph(data);
    });
  }

  ngOnInit() {
    const innerWidth = window.innerWidth;
    const innerHeight = window.innerHeight;
    const ratio = innerWidth / innerHeight;
   // this.view = [innerWidth * 0.43 , innerHeight * 0.69];
    /* if ( ratio >= 0 && ratio <= 1.3) {
      this.view = [innerWidth / 1.5, innerHeight / 1.5 ];
    } else if ( ratio > 1.3 && ratio <= 1.4)  {
    this.view = [innerWidth / 2.1, innerHeight / 1.5 ];
    } else if ( ratio > 1.4 && ratio <= 1.5)  {
      this.view = [innerWidth / 3.45, innerHeight / 1.5 ];
    } else if ( ratio > 1.5 && ratio <= 1.6)  {
      this.view = [innerWidth / 2.15, innerHeight / 1.5 ];
    } else if ( ratio > 1.7 && ratio <= 1.8)  {
      this.view = [innerWidth / 2.15, innerHeight / 1.5 ];
    }  else if ( ratio > 1.9 && ratio <= 2.0)  {
      this.view = [innerWidth / 2.15, innerHeight / 1.5 ];
    } else {
    this.view = [innerWidth / 2.25, 400];
    } */
  }
  onResize(event) {
    const innerWidth = event.target.innerWidth;
    const innerHeight = event.target.innerHeight;
    const ratio = innerWidth / innerHeight;
   // this.view = [innerWidth * 0.43 , innerHeight * 0.69];
    /* if ( ratio >= 0 && ratio <= 1.3) {
      this.view = [innerWidth / 1.5, innerHeight / 1.5 ];
    } else if ( ratio > 1.3 && ratio <= 1.4)  {
    this.view = [innerWidth / 1.8, innerHeight / 1.5 ];
    } else if ( ratio > 1.4 && ratio <= 1.5)  {
      this.view = [innerWidth / 3.45, innerHeight / 1.5 ];
    } else if ( ratio > 1.5 && ratio <= 1.6)  {
      this.view = [innerWidth / 2.15, innerHeight / 1.5 ];
    } else if ( ratio > 1.7 && ratio <= 1.8)  {
      this.view = [innerWidth / 2.15, innerHeight / 1.5 ];
    }  else if ( ratio > 1.9 && ratio <= 2.0)  {
      this.view = [innerWidth / 2.15, innerHeight / 1.5 ];
    } else {
    this.view = [innerWidth / 2.25, 400];
    } */
}
}
