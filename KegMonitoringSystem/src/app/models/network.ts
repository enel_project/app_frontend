class Trend {
    name: string;
    series: TrendAmount[];
}
class TrendAmount {
name: any;
x: any;
y: any;
r: any;
}
class BattAmount {
    name: any;
    value: number;
    }

export class NetworkObject {
    name: string;
    network: string;
    kegId: any [];
    trendGraph: Trend [];
    saleGraph: BattAmount [];
    batteryGraph: BattAmount[];
    amountGraph: BattAmount [];
    constructor() {
        this.kegId = [];
        this.batteryGraph = [];
        this.amountGraph = [];
        this.saleGraph = [];
        this.trendGraph = [];
    }


TrendGraph(object) {
    this.trendGraph = [];
    let index = 1;
    object.kegs.forEach(keg => {
        const temp = new Trend();
        temp.series = [];
        temp.name = keg.name;
        keg.sensors.forEach(element => {
           temp.series.push({
               name: ' ',
               x : new Date(element.timestamp),
               y: index, // keg.name,
               r: element.level,
           });
        });
        this.trendGraph.push(temp);
        index++;
        });
    }
SaleGraph(object) {
    this.saleGraph = [];
    object.kegs.forEach(element => {
        const  temp = new BattAmount();
        let sum = 0;
        element.sensors.forEach( data => {
            sum += data.level;
        });
        temp.name = element.name;
        temp.value = sum;
        this.saleGraph.push(temp);
    });
}

BatteryGraph(object) {
    this.batteryGraph = [];
    object.kegs.forEach(element => {
        const  temp = new BattAmount();
        temp.name = element.name;
        if (element.sensors  && element.sensors.length > 0  ) {
        temp.value = ((element.sensors[(element.sensors.length - 1)].voltage * 1.52) / 5) * 100;
        } else {
            temp.value = 0;
        }
        this.batteryGraph.push(temp);
    });
}
AmountGraph(object) {
    this.amountGraph = [];
    object.kegs.forEach(element => {
        const kegPercent = ((element.size - element.consumed) / element.size) * 100;
        this.amountGraph.push({
         name : element.name,
         value: kegPercent
     });
    });
}
}
