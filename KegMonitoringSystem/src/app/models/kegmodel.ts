
export class KegObject {
    battery: number;
    lastReset: string;
    name: string;
    size: number;
    _id: string;
    temperature: number;
    voltage: number;
    amountRemaining: number;

    constructor() {
    }
}
