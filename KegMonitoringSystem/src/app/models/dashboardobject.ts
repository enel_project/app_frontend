
import { NetworkObject} from './network';

export class DashboardObject {
    name: string;
    network: NetworkObject [];
}
