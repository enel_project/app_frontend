import { TestBed } from '@angular/core/testing';

import { JWTIntercepterService } from './jwtintercepter.service';

describe('JWTIntercepterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JWTIntercepterService = TestBed.get(JWTIntercepterService);
    expect(service).toBeTruthy();
  });
});
