import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthenticationService } from '../services/auth-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JWTIntercepter implements HttpInterceptor {

  constructor( private authService: AuthenticationService) { }

  intercept ( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
    const currentUser = this.authService.currentUserValue;
    if ( currentUser && currentUser.token) {
      request = request.clone({
        headers: request.headers.set( 'x-access-token', `${currentUser.token}`)
     });
   }

    return next.handle(request);

    }
}

