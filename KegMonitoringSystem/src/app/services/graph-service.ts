import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { GraphObject } from '../models/graphobject';
import { KegObject } from '../models/kegmodel';


@Injectable( { providedIn: 'root' } )
export class GraphService  {
    config = 'http://35.229.63.41/api/user/graph/';
    constructor(private http: HttpClient) {
    }
    networkGraph(graphObject: GraphObject, network: string) {
       return  this.http.get<any>(this.config + network, {
            params: {
                date: graphObject.date.toString(),
                range: graphObject.range,
            },
        });
    }

    userGraph(graphObject: GraphObject) {
        return  this.http.get<any>(this.config, {
             params: {
                 date: graphObject.date.toString(),
                 range: graphObject.range,
             },
         });
     }

     KegGraph (graphObject: GraphObject, network: string, keg: string) {
          return this.http.get<any>(this.config +  network + '/' + keg, {
             params: {
                 date: graphObject.date.toString(),
                 range: graphObject.range,
             },
                });
        }
}
