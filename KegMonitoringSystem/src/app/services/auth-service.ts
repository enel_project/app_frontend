import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';

@Injectable( { providedIn: 'root' } )
export class AuthenticationService  {

    private currentUserSubject: BehaviorSubject<User>;
    private loggedInMain: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private loggedInLanding: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
    public currentUser: Observable<User>;
    config = 'http://35.229.63.41/api';

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();

        if (this.currentUserSubject.value !== null) {
            this.loggedInMain.next(true);
            this.loggedInLanding.next(false);
        }
    }

     public get isLoggedInMain() {
        return this.loggedInMain.asObservable();
    }
    public get isLoggedInLanding() {
        return this.loggedInLanding.asObservable();
    }
    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(name: string, password: string) {
        return this.http.post<any>(`${this.config}/authenticate`, { name, password })
            .pipe(map(user => {
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                    this.loggedInMain.next(true);
                    this.loggedInLanding.next(false);
                }

                return user;
            }));
    }

    logout() {
        const token = this.currentUserValue.token;
        const headers = new HttpHeaders().set('x-access-token', token);
        localStorage.removeItem('currentUser');
        localStorage.removeItem('networkId');
        localStorage.removeItem('kegId');
        this.currentUserSubject.next(null);
        this.loggedInMain.next(false);
        this.loggedInLanding.next(true);
        return this.http.delete<any>(`${this.config}/authenticate`, {headers});
    }
}
