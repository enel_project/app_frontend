import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GraphObject } from '../models/graphobject';


@Injectable( { providedIn: 'root' } )
export class NetworkService  {

    config = 'http://35.229.63.41/api/user/';

    constructor(private http: HttpClient) {
    }
    changeKegName(name: string, kegId: string, networkId: string) {
        const request = {
            key: 'name',
            value: name,
        };
        return this.http.patch<any>(this.config + networkId + '/' + kegId, request );
    }
    changeKegSize(name: number, kegId: string, networkId: string) {
        const request = {
            key: 'size',
            value: name,
        };
        return this.http.patch<any>(this.config + networkId + '/' + kegId, request );
    }
    ResetKeg(name: string, kegId: string, networkId: string) {
        const request = {
            key: 'reset',
            value: name,
        };
        return this.http.patch<any>(this.config + networkId + '/' + kegId, request );
    }

}
