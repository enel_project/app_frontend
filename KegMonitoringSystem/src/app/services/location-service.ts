import { Injectable } from '@angular/core';


@Injectable( { providedIn: 'root' } )
export class LocationService  {
    public data: any;
    public kegpage: {
        kedId;
        networkId;
    };
    public constructor() {
        if (typeof this.data === 'undefined') {
            this.data = JSON.parse(localStorage.getItem('networkId'));
           } else {
               localStorage.setItem('networkId', JSON.stringify(this.data));
           }
    }
    public store() {
    if (typeof this.data === 'undefined') {
        this.data = JSON.parse(localStorage.getItem('networkId'));
       } else {
           localStorage.setItem('networkId', JSON.stringify(this.data));
       }
    }

}


