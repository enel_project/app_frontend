import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GraphService } from '../services/graph-service';
import { LocationService } from '../services/location-service';
import { NetworkObject } from '../models/network';
import {NetworkService } from '../services/network-service';
import { KegObject } from '../models/kegmodel';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit {
  private locationName: BehaviorSubject<string>;
  place: string;
  closeResult: string;
  network: NetworkObject;
  kegObject: KegObject;
  networkId;
  beerName: string;
  beerSize: number;
  index: number;
  object;
  object1;
  date;
  interval;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private location: LocationService,
    private graph: GraphService,
    private networkService: NetworkService,
  ) {
    this.network = new NetworkObject();
    this.kegObject = new KegObject();
   }

  ngOnInit() {
    this.interval = setInterval(() => {
      this.refreshPage();
  }, 20000);
    this.route.params.subscribe(( params => {
      this.locationName = new BehaviorSubject<string>(params['location']);
      this.place = this.currentLocation;
  }));
    this.networkId = this.location.data;
    this.date = new Date();
    this.date.setDate(this.date.getDate() + 1 );
    this.date.setHours(10);
    this.date.setMinutes(0);
    this.object = {
      date: this.date.getTime(),
      range: '1d'
    };
    this.graph.networkGraph(this.object, this.networkId ).subscribe(data => {
      this.network.name = data.name;
      this.network.network = data.network;
      data.kegs.forEach(element => {
        this.network.kegId.push(element.keg);
      });
      this.network.AmountGraph(data);
      this.network.BatteryGraph(data);
      this.network.SaleGraph(data);
      this.network.TrendGraph(data);
    });
  }
    public get currentLocation(): string {
      return this.locationName.value;
  }

  open(content, i , x) {
    this. object1 = {
      date: this.date.getTime(),
      range: '1y'
    };
    if ( x === 1) {
      this.kegObject.name = '';
      this.kegObject.lastReset = '';
      this.kegObject.size = null;
      this.kegObject.temperature = null;
      this.kegObject.voltage = null;
      this.graph.KegGraph(this.object1, this.networkId, this.network.kegId[i] ).subscribe(
        data => {
          if (data.sensors.length > 0) {
              this.kegObject.name = data.name;
              this.kegObject.lastReset = new Date(data.reset).toString().slice(0, 24);
              this.kegObject.size = data.size;
              this.kegObject._id = data._id;
              this.kegObject.temperature = data.sensors[(data.sensors.length - 1)].temperature;
              this.kegObject.voltage = (data.sensors[(data.sensors.length - 1)].voltage / 3.3) * 100;
              this.kegObject.amountRemaining = ((data.size - data.consumed) / data.size) * 100;
          } else {
            this.kegObject.name = data.name;
            this.kegObject.lastReset = new Date(data.reset).toString().slice(0, 24);
            this.kegObject.size = data.size;
            this.kegObject._id = data._id;
            this.kegObject.amountRemaining = ((data.size - data.consumed) / data.size) * 100;
          }
        }
      );

    }
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
    this.index = i;
  }

  changeName() {
    if (this.beerName) {
      this.networkService.changeKegName(this.beerName, this.network.kegId[this.index], this.network.network).subscribe(data => {
        this.beerName = '';
      this.refreshPage();
      });
    }
    if (this.beerSize) {
        this.networkService.changeKegSize( this.beerSize , this.network.kegId[this.index], this.network.network).subscribe( data => {
        this.beerSize = null;
        this.refreshPage();
     });
    }
  }

  refreshPage() {
    this.graph.networkGraph(this.object, this.networkId ).subscribe(data => {
      this.network.name = data.name;
      this.network.network = data.network;
      this.network.AmountGraph(data);
      this.network.BatteryGraph(data);
      this.network.SaleGraph(data);
      this.network.TrendGraph(data);
      data.kegs.forEach(element => {
        this.network.kegId.push(element.keg);
      });
    });
  }

  resetKeg() {
    const resetDate = new Date();
    const resetDateString = resetDate.toString();
    this.networkService.ResetKeg(resetDateString, this.network.kegId[this.index], this.network.network).subscribe( data => {
    this.refreshPage();
    });
  }
}

