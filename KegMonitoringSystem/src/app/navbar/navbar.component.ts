import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth-service';
import { Observable} from 'rxjs';
import { User } from '../models/user';
import { map} from 'rxjs/operators';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedInMain$: Observable<boolean>;
  isLoggedInLanding$: Observable<boolean>;
  username: User;

  constructor(private auth: AuthenticationService,
              private router: Router ) {
   }
   ngOnInit() {
    this.isLoggedInMain$ = this.auth.isLoggedInMain;
    this.isLoggedInLanding$ = this.auth.isLoggedInLanding;
    const currentUser = this.auth.currentUser.pipe(
      map(value => this.setUsername(value)
      ));
      const subscribe = currentUser.subscribe(val => 1);
  }

  setUsername(value: User) {
    this.username = value;
  }

  onLogout() {
    this.auth.logout().subscribe( data => {
      this.router.navigate(['/login']);
    });
  }
}

