import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/auth-service';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  rememberme;
  loginForm: FormGroup;
  submitted = false;

  constructor( private authservice: AuthenticationService, private router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.minLength(2), Validators.required]),
      rememberme: new FormControl(false)
    });

    this.rememberme = JSON.parse(localStorage.getItem('rememberme'));
    if (this.rememberme !== null) {
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(this.rememberme);
     this.loginForm.get('username').setValue(decodedToken.name);
    this.loginForm.get('password').setValue(this.rememberme);
    }
  }

  ngOnInit() {
  }
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
   }

    this.authservice.login(this.loginForm.get('username').value, this.loginForm.get('password').value).subscribe( (user: User) => {
      if (this.loginForm.get('rememberme').value === true) {
      localStorage.setItem('rememberme', JSON.stringify(user.token));
      } else {
        localStorage.removeItem('rememberme');
      }
      this.router.navigate(['/dashboard']);
    });
  }

  get f() { return this.loginForm.controls; }

}
