import { Component, OnInit } from '@angular/core';
import { Router, Route } from '@angular/router';
import { LocationService } from '../services/location-service';
import { GraphService } from '../services/graph-service';
import { GraphObject } from '../models/graphobject';
import { DashboardObject } from '../models/dashboardobject';
import { NetworkObject } from '../models/network';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dashboard: DashboardObject;
  TotalBeer: number;
  currentGraph: NetworkObject;

  constructor(
    private router: Router,
    private location: LocationService,
    private graphservice: GraphService,
  ) {
    this.dashboard = new DashboardObject();
    this.currentGraph = new NetworkObject();
    this.TotalBeer = 0;
   }

  ngOnInit() {
    const date1 = new Date();
    const curr_hour = '1d';
    const graph = new GraphObject ();
    date1.setDate(date1.getDate() + 1);
    date1.setHours(10);
    date1.setMinutes(0);
    graph.date = date1.getTime();
    graph.range = curr_hour;
    this.graphservice.userGraph(graph).subscribe(data => {
      this.dashboard.name = data.name;
     this.dashboard.network = [];
     for ( let i = 0; i < data.networks.length; i++) {
      const temp_network = new NetworkObject();
      data.networks[i].kegs.forEach(element => {
        temp_network.kegId.push(element.keg);
      });
      temp_network.network =  data.networks[i].network;
      temp_network.name =  data.networks[i].name;
      temp_network.BatteryGraph(data.networks[i]);
      temp_network.SaleGraph(data.networks[i]);
      temp_network.AmountGraph(data.networks[i]);
      temp_network.TrendGraph(data.networks[i]);
      this.dashboard.network.push(temp_network);
      if (i === 0 ) {
        this.currentGraph = this.dashboard.network[0];
      }
    }
     this.dashboard.network.forEach( network => {
      network.amountGraph.forEach(sum => {
        this.TotalBeer += sum.value;
      });
     });
    });
  }

  navLocation( networkId, name) {
    this.location.data = networkId;
    this.location.store();
    this.router.navigate(['/network', name]);
  }
  showGraph(network: NetworkObject) {
    this.currentGraph = network;
  }

}
