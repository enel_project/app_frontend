export const multi: any[] = [
    {
      "name": "Bosnia and Herzegovina",
      "series": [
        {
          "value": 5322,
          "name": "2016-09-16T09:30:55.910Z"
        },
        {
          "value": 3897,
          "name": "2016-09-21T21:21:03.484Z"
        },
        {
          "value": 3469,
          "name": "2016-09-16T16:53:40.337Z"
        },
        {
          "value": 3634,
          "name": "2016-09-13T03:35:22.407Z"
        },
        {
          "value": 6487,
          "name": "2016-09-20T12:49:55.658Z"
        }
      ]
    },
    {
      "name": "Singapore",
      "series": [
        {
          "value": 2168,
          "name": "2016-09-16T09:30:55.910Z"
        },
        {
          "value": 5399,
          "name": "2016-09-21T21:21:03.484Z"
        },
        {
          "value": 2079,
          "name": "2016-09-16T16:53:40.337Z"
        },
        {
          "value": 4320,
          "name": "2016-09-13T03:35:22.407Z"
        },
        {
          "value": 5254,
          "name": "2016-09-20T12:49:55.658Z"
        }
      ]
    },
    {
      "name": "Mexico",
      "series": [
        {
          "value": 4170,
          "name": "2016-09-16T09:30:55.910Z"
        },
        {
          "value": 4707,
          "name": "2016-09-21T21:21:03.484Z"
        },
        {
          "value": 3813,
          "name": "2016-09-16T16:53:40.337Z"
        },
        {
          "value": 3853,
          "name": "2016-09-13T03:35:22.407Z"
        },
        {
          "value": 5609,
          "name": "2016-09-20T12:49:55.658Z"
        }
      ]
    },
    {
      "name": "Faroe Islands",
      "series": [
        {
          "value": 3297,
          "name": "2016-09-16T09:30:55.910Z"
        },
        {
          "value": 4061,
          "name": "2016-09-21T21:21:03.484Z"
        },
        {
          "value": 2395,
          "name": "2016-09-16T16:53:40.337Z"
        },
        {
          "value": 2017,
          "name": "2016-09-13T03:35:22.407Z"
        },
        {
          "value": 6851,
          "name": "2016-09-20T12:49:55.658Z"
        }
      ]
    },
    {
      "name": "Kazakhstan",
      "series": [
        {
          "value": 5705,
          "name": "2016-09-16T09:30:55.910Z"
        },
        {
          "value": 3603,
          "name": "2016-09-21T21:21:03.484Z"
        },
        {
          "value": 4253,
          "name": "2016-09-16T16:53:40.337Z"
        },
        {
          "value": 2595,
          "name": "2016-09-13T03:35:22.407Z"
        },
        {
          "value": 4761,
          "name": "2016-09-20T12:49:55.658Z"
        }
      ]
    }
  ];
  export const single: any[] = [
    {
      name: 'Germany',
      value: 8940000
    },
    {
      name: 'USA',
      value: 5000000
    },
    {
      name: 'France',
      value: 7200000
    }
  ];